package affablebean.web.controllers;

import affablebean.business.dto.ShoppingCart;
import affablebean.business.entity.Category;
import affablebean.business.entity.Product;
import affablebean.business.manager.CategoryManager;
import affablebean.business.manager.ProductManager;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Map;
import java.util.Set;

/**
 * Created by javad on 1/9/14.
 */


@Controller
public class FrontEndController {

    private final static Logger log = LoggerFactory.getLogger(FrontEndController.class);

    @Autowired
    private ServletContext servletContext;

    @Autowired
    private CategoryManager categoryManager;

    @Autowired
    private ProductManager productManager;

    @PostConstruct
    public void init() {
        servletContext.setAttribute("categories", categoryManager.findAllCategory());

    }


    @RequestMapping(value = "/category", method = RequestMethod.GET)
    public String getCategory(HttpServletRequest request, @RequestParam Map<String, String> parameters, Model model) {
        HttpSession httpSession = request.getSession();

        if (StringUtils.isNotEmpty(parameters.get("category_id"))) {
            Long categoryID = Long.valueOf(parameters.get("category_id"));

            Category category = categoryManager.findCategory(categoryID);
            Set<Product> products = categoryManager.findProducts(categoryID);
            httpSession.setAttribute("selectedCategory", category);
            httpSession.setAttribute("selectedProducts", products);
        }

        return "/category";
    }

    @RequestMapping(value = "/checkout", method = RequestMethod.GET)
    public String checkOut(HttpServletRequest request) {
        // TODO: Implement checkout page request
        return null;
    }

    @RequestMapping(value = "/chooseLanguage", method = RequestMethod.GET)
    public String chooseLanguage(HttpServletRequest request) {
        String lanquageParameter = request.getParameter("language");
        return "/index";
    }

    @RequestMapping(value = {"/index**", "/"}, method = {RequestMethod.GET, RequestMethod.POST})
    public String homePage(HttpServletRequest request) {

        return "/index";
    }

    @RequestMapping(value = "/addToCart", method = RequestMethod.POST)
    public String addToCart(HttpServletRequest request, @RequestParam("product_id") Integer productID) {
        final Product product = productManager.findProductByID(productID);
        ShoppingCart shoppingCart;
        HttpSession httpSession = request.getSession();

        if (httpSession.getAttribute("shoppingCart") == null) {
            shoppingCart = new ShoppingCart();
            httpSession.setAttribute("shoppingCart", shoppingCart);
        }

        shoppingCart = (ShoppingCart) httpSession.getAttribute("shoppingCart");
        shoppingCart.addItemToCart(product);

        httpSession.setAttribute("shoppingCart", shoppingCart);

        return "/category";
    }


    @RequestMapping(value = "/updateCart", method = RequestMethod.POST)
    public String updateCart(HttpServletRequest request, @RequestParam Map<String, String> parameters) {
        HttpSession httpSession = request.getSession();
        ShoppingCart shoppingCart;

        if (httpSession.getAttribute("shoppingCart") == null) {
            shoppingCart = new ShoppingCart();
            httpSession.setAttribute("shoppingCart", shoppingCart);
        }


        if (StringUtils.isNotEmpty(parameters.get("quantity"))) {
            int quantity = Integer.valueOf(parameters.get("quantity"));
            int productID = Integer.valueOf(parameters.get("productId"));
            shoppingCart = (ShoppingCart) httpSession.getAttribute("shoppingCart");
            shoppingCart.setQuantity(productID, quantity);
        }

        return "/cart";
    }

    @RequestMapping(value = "/viewCart", method = RequestMethod.GET)
    public String viewCart(@RequestParam Map<String, String> parameters, HttpServletRequest request) {
        HttpSession httpSession = request.getSession();
        ShoppingCart shoppingCart;

        if (httpSession.getAttribute("shoppingCart") == null) {
            shoppingCart = new ShoppingCart();
            httpSession.setAttribute("shoppingCart", shoppingCart);
        }


        if (StringUtils.isNotEmpty(parameters.get("clear"))) {
            if (parameters.get("clear").equals("true")) {
                shoppingCart = (ShoppingCart) httpSession.getAttribute("shoppingCart");
                shoppingCart.clearCart();
            }
        }

        return "/cart";
    }

    @RequestMapping(value = "/purchase", method = RequestMethod.POST)
    public String purchase(HttpServletRequest request) {
        // TODO: Implement purchase action
        return null;
    }


}
