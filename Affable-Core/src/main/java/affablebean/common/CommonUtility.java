package affablebean.common;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by javad on 2/12/2015.
 */
public class CommonUtility {
    public static String getApplicationAddress(HttpServletRequest request) {
        return getIP4LocalAddress(request.getLocalAddr()) + ":" + request.getLocalPort() + request.getContextPath();
    }


    private static String getIP4LocalAddress(String address) {
        if (address.equalsIgnoreCase("0:0:0:0:0:0:0:1"))
            return "127.0.0.1";
        else {
            return address;
        }
    }
}
