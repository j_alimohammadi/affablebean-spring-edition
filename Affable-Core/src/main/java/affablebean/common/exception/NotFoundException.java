package affablebean.common.exception;

/**
 * Created by javad on 2/6/2015.
 */
public class NotFoundException extends RuntimeException {
    public NotFoundException(String errorMessage) {
        super(errorMessage);
    }
}
