package affablebean.common.exception;

/**
 * Created by javad on 2/11/2015.
 */
public class ApplicationInternalError extends RuntimeException {
    public ApplicationInternalError(String message) {
        super(message);
    }

    public ApplicationInternalError(Throwable cause) {
        super(cause);
    }
}
