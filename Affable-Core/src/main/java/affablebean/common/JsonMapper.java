package affablebean.common;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * Created by javad on 2/11/2015.
 */

public class JsonMapper {
    private final static ObjectMapper jsonMapper = new ObjectMapper();

    public static <T> String createJsonFromObject(T object) throws JsonProcessingException {
        return jsonMapper.writerWithDefaultPrettyPrinter().writeValueAsString(object);
    }
}
