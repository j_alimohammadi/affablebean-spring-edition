package affablebean.dataaccess;

import affablebean.business.entity.Category;
import affablebean.dataaccess.generic.AbstractHbnDao;
import org.springframework.stereotype.Repository;

/**
 * Created by javad on 2/11/2015.
 */
@Repository
public class CategoryDaoImp extends AbstractHbnDao<Category> implements CategoryDao {
}
