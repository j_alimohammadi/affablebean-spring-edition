package affablebean.dataaccess;

import affablebean.business.entity.Product;
import affablebean.dataaccess.generic.AbstractHbnDao;
import org.springframework.stereotype.Repository;

/**
 * Created by javad on 7/9/2015.
 */
@Repository
public class ProductDaoImp extends AbstractHbnDao<Product> implements ProductDao {
}
