package affablebean.dataaccess;

import affablebean.business.entity.Product;
import affablebean.dataaccess.generic.Dao;

/**
 * Created by javad on 7/9/2015.
 */
public interface ProductDao extends Dao<Product> {
}
