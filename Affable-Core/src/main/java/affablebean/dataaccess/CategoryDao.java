package affablebean.dataaccess;

import affablebean.business.entity.Category;
import affablebean.dataaccess.generic.Dao;

/**
 * Created by javad on 2/11/2015.
 */
public interface CategoryDao extends Dao<Category> {
}
