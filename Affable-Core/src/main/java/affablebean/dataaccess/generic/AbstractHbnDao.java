package affablebean.dataaccess.generic;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.util.List;

/**
 * Created by javad on 1/3/14.
 */
public abstract class AbstractHbnDao<T> implements Dao<T> {
    @Autowired
    private SessionFactory sessionFactory;


    private Class<T> domainClass;

    protected Session getSession() {
        return sessionFactory.getCurrentSession();
    }

    private String getDomainClass() {
        if (domainClass == null) {
            ParameterizedType thisType = (ParameterizedType) getClass().getGenericSuperclass();
            this.domainClass = (Class<T>) thisType.getActualTypeArguments()[0];
        }

        return domainClass.getName();
    }

    @Override
    public Serializable create(T object) {
        return getSession().save(object);
    }

    @Override
    public T get(Serializable id) {
        return (T) getSession().get(getDomainClass(), id);
    }

    @Override
    public T load(Serializable id) {
        return (T) getSession().load(getDomainClass(), id);
    }

    @Override
    public List<T> getAll() {
        return getSession().createQuery("FROM " + getDomainClass()).list();
    }

    @Override
    public void update(T t) {
        getSession().update(t);
    }

    @Override
    public void delete(T t) {
        getSession().delete(t);
    }

    @Override
    public void deleteById(Serializable id) {
        delete(load(id));
    }

    @Override
    public long deleteAll() {
        return getSession().createQuery("DELETE " + getDomainClass()).executeUpdate();
    }

    @Override
    public long count() {
        return (Long) getSession().createQuery("SELECT COUNT(*) FROM " + getDomainClass()).uniqueResult();
    }

    @Override
    public boolean exists(Serializable id) {
        return get(id) == null;
    }
}
