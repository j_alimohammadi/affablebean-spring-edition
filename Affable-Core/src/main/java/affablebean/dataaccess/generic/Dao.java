package affablebean.dataaccess.generic;

import java.io.Serializable;
import java.util.List;

/**
 * Created by javad on 1/3/14.
 */
public interface Dao<T> {

    public Serializable create(T object);

    public T get(Serializable id);

    public T load(Serializable id);

    public List<T> getAll();

    public void update(T t);

    public void delete(T t);

    public void deleteById(Serializable id);

    public long deleteAll();

    public long count();

    public boolean exists(Serializable id);
}
