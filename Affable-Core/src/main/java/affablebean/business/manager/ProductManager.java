package affablebean.business.manager;

import affablebean.business.entity.Product;

/**
 * Created by javad on 7/9/2015.
 */
public interface ProductManager {
    Product findProductByID(Integer productID);
}
