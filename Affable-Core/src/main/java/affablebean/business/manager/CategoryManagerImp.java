package affablebean.business.manager;

import affablebean.business.entity.Category;
import affablebean.business.entity.Product;
import affablebean.common.exception.NotFoundException;
import affablebean.dataaccess.CategoryDao;
import org.hibernate.Hibernate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Set;

/**
 * Created by javad on 2/6/2015.
 */
@Service
public class CategoryManagerImp implements CategoryManager {
    @Autowired
    private CategoryDao categoryDao;

    @Override
    @Transactional
    public List<Category> findAllCategory() {
        return categoryDao.getAll();
    }

    @Override
    @Transactional
    public Category findCategory(Long categoryID) throws IllegalArgumentException {
        Category result;
        if (categoryID != null) {
            result = categoryDao.get(categoryID);
            if (result == null) {
                throw new NotFoundException(String.format("Category with id: %s not found", categoryID));
            } else {
                return result;
            }
        } else {
            throw new IllegalArgumentException("Category id is null");
        }
    }

    @Override
    @Transactional
    public Set<Product> findProducts(Long categoryID) throws NotFoundException, IllegalArgumentException {
        Category selectedCategory = findCategory(categoryID);
        Hibernate.initialize(selectedCategory.getProducts());
        return selectedCategory.getProducts();
    }


}
