package affablebean.business.manager;

import affablebean.business.entity.Product;
import affablebean.dataaccess.ProductDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;

/**
 * Created by javad on 7/9/2015.
 */
@Service
public class ProductManagerImp implements ProductManager {
    @Autowired
    private ProductDao productDao;

    @Transactional
    @Override
    public Product findProductByID(Integer productID) {
        return productDao.get(productID);
    }


}
