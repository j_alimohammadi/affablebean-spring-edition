package affablebean.business.manager;

import affablebean.business.entity.Category;
import affablebean.business.entity.Product;
import affablebean.common.exception.NotFoundException;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Set;

/**
 * Created by javad on 7/9/2015.
 */
public interface CategoryManager {
    List<Category> findAllCategory();

    Category findCategory(Long categoryID) throws IllegalArgumentException;

    Set<Product> findProducts(Long categoryID) throws NotFoundException, IllegalArgumentException;
}
