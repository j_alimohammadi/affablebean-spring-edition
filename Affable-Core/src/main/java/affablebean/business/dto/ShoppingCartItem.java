package affablebean.business.dto;

import affablebean.business.entity.Product;

import java.math.BigDecimal;

/**
 * Created by javad on 7/21/2015.
 */
public class ShoppingCartItem {
    private Product product;
    private int quantity;
    private BigDecimal subTotalPrice;

    public ShoppingCartItem(Product product) {
        this.product = product;
        this.quantity = 1;
        this.subTotalPrice = product.getPrice();
    }

    public void addOneQuantity() {
        quantity++;
        BigDecimal value = BigDecimal.valueOf(quantity);
        subTotalPrice = product.getPrice().multiply(value);
    }

    public void updateQuantity(int quantity) {
        this.quantity = quantity;
        BigDecimal value = BigDecimal.valueOf(quantity);
        subTotalPrice = product.getPrice().multiply(value);

    }

    public Product getProduct() {
        return product;
    }

    public String getSubTotalPrice() {
        return subTotalPrice.toPlainString();
    }


    public int getQuantity() {
        return quantity;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ShoppingCartItem that = (ShoppingCartItem) o;

        return product.getId().equals(that.product.getId());
    }

    @Override
    public int hashCode() {
        return product.hashCode();
    }
}
