package affablebean.business.dto;

import affablebean.business.entity.Product;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by javad.alimohammadi on 7/9/2015.
 */
public class ShoppingCart {
    private Set<ShoppingCartItem> itemsInCart = new HashSet<>();

    public Set<ShoppingCartItem> getItemsInCart() {
        return Collections.unmodifiableSet(itemsInCart);
    }

    public synchronized boolean isShoppingCartEmpty() {
        return itemsInCart.isEmpty();
    }

    public synchronized int getShoppingCartItemCount() {
        int shoppingItemCount = 0;
        for (ShoppingCartItem shoppingCartItem : itemsInCart) {
            shoppingItemCount += shoppingCartItem.getQuantity();
        }

        return shoppingItemCount;
    }


    public synchronized void addItemToCart(Product item) {
        ShoppingCartItem newShoppingCartItem = new ShoppingCartItem(item);
        if (itemsInCart.contains(newShoppingCartItem)) {
            for (ShoppingCartItem shoppingCartItem : itemsInCart) {
                if (shoppingCartItem.equals(newShoppingCartItem)) {
                    shoppingCartItem.addOneQuantity();
                }
            }
        } else {
            itemsInCart.add(newShoppingCartItem);
        }
    }

    public synchronized void setQuantity(Integer productID, int quantity) {
        for (ShoppingCartItem shoppingCartItem : itemsInCart) {
            if (shoppingCartItem.getProduct().getId().equals(productID)) {
                if (quantity < 1) {
                    itemsInCart.remove(shoppingCartItem);
                } else {
                    shoppingCartItem.updateQuantity(quantity);
                }
                break;
            }
        }

    }

    public String getTotalPrice() {
        BigDecimal totalPrice = BigDecimal.ZERO;
        for (ShoppingCartItem shoppingCartItem : itemsInCart) {
            totalPrice = totalPrice.add(new BigDecimal(shoppingCartItem.getSubTotalPrice()));
        }

        return totalPrice.toPlainString();
    }

    public synchronized void clearCart() {
        itemsInCart.clear();
    }


}
