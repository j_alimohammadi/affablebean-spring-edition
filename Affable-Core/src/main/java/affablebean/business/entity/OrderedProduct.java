package affablebean.business.entity;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by javad on 1/2/2015.
 */
@Entity
@Table(name = "ordered_product", catalog = "affablebean")
public class OrderedProduct {
    @EmbeddedId
    private OrderedProductPK orderedProductPK;

    @Column(name = "quantity", nullable = false, insertable = true, updatable = true)
    private Short quantity;

    @ManyToOne
    @JoinColumn(name = "product_id", insertable = false, updatable = false)
    private Product product;

    @ManyToOne
    @JoinColumn(name = "customer_order_id", insertable = false, updatable = false)
    private CustomerOrder customerOrder;

    public Short getQuantity() {
        return quantity;
    }

    public void setQuantity(Short quantity) {
        this.quantity = quantity;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public CustomerOrder getCustomerOrder() {
        return customerOrder;
    }

    public void setCustomerOrder(CustomerOrder customerOrder) {
        this.customerOrder = customerOrder;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        OrderedProduct that = (OrderedProduct) o;

        if (customerOrder != null ? !customerOrder.equals(that.customerOrder) : that.customerOrder != null)
            return false;
        if (!orderedProductPK.equals(that.orderedProductPK)) return false;
        if (product != null ? !product.equals(that.product) : that.product != null) return false;
        if (quantity != null ? !quantity.equals(that.quantity) : that.quantity != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = orderedProductPK.hashCode();
        result = 31 * result + (quantity != null ? quantity.hashCode() : 0);
        result = 31 * result + (product != null ? product.hashCode() : 0);
        result = 31 * result + (customerOrder != null ? customerOrder.hashCode() : 0);
        return result;
    }
}

@Embeddable
class OrderedProductPK implements Serializable {
    @Column(name = "customer_order_id")
    private Integer customerOrderId;

    @Column(name = "product_id")
    private Integer productId;


    public Integer getCustomerOrderId() {
        return customerOrderId;
    }

    public void setCustomerOrderId(Integer customerOrderId) {
        this.customerOrderId = customerOrderId;
    }

    public Integer getProductId() {
        return productId;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        OrderedProductPK that = (OrderedProductPK) o;

        if (customerOrderId != null ? !customerOrderId.equals(that.customerOrderId) : that.customerOrderId != null)
            return false;
        if (productId != null ? !productId.equals(that.productId) : that.productId != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = customerOrderId != null ? customerOrderId.hashCode() : 0;
        result = 31 * result + (productId != null ? productId.hashCode() : 0);
        return result;
    }
}