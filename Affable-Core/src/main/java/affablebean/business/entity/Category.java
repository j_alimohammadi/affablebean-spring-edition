package affablebean.business.entity;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by javad on 1/2/2015.
 */
@Entity
@Table(name = "category", schema = "affablebean")
public class Category {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name", nullable = false, insertable = true, updatable = true, length = 45)
    private String name;

    @Column(name = "img_address", nullable = false, insertable = true, updatable = true, length = 500)
    private String imgAddress;

    @OneToMany(mappedBy = "category")
    private Set<Product> products = new HashSet<>();

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImgAddress() {
        return imgAddress;
    }

    public void setImgAddress(String imgAddress) {
        this.imgAddress = imgAddress;
    }

    public Set<Product> getProducts() {
        return products;
    }

    public void setProducts(Set<Product> products) {
        this.products = products;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Category category = (Category) o;

        if (!imgAddress.equals(category.imgAddress)) return false;
        if (!id.equals(category.id)) return false;
        if (!name.equals(category.name)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id.hashCode();
        result = 31 * result + name.hashCode();
        result = 31 * result + imgAddress.hashCode();
        return result;
    }
}
