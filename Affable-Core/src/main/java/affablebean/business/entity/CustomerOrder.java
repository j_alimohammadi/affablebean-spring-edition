package affablebean.business.entity;

import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by javad on 1/2/2015.
 */
@Entity
@Table(name = "customer_order", catalog = "affablebean")
public class CustomerOrder {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "amount", nullable = false, insertable = true, updatable = true, precision = 2)
    private BigDecimal amount;

    @Column(name = "date_created", nullable = false, insertable = true, updatable = true)
    private Timestamp dateCreated;

    @Column(name = "confirmation_number", nullable = false, insertable = true, updatable = true)
    private Integer confirmationNumber;

    @OneToMany(mappedBy = "customerOrder")
    private Set<OrderedProduct> orderedProducts = new HashSet<>();

    @ManyToOne
    @JoinColumn(name = "customer_id")
    private Customer customer;


    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public Timestamp getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Timestamp dateCreated) {
        this.dateCreated = dateCreated;
    }

    public Integer getConfirmationNumber() {
        return confirmationNumber;
    }

    public void setConfirmationNumber(Integer confirmationNumber) {
        this.confirmationNumber = confirmationNumber;
    }

    public Set<OrderedProduct> getOrderedProducts() {
        return orderedProducts;
    }

    public void setOrderedProducts(Set<OrderedProduct> orderedProducts) {
        this.orderedProducts = orderedProducts;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CustomerOrder that = (CustomerOrder) o;

        if (amount != null ? !amount.equals(that.amount) : that.amount != null) return false;
        if (confirmationNumber != null ? !confirmationNumber.equals(that.confirmationNumber) : that.confirmationNumber != null)
            return false;
        if (customer != null ? !customer.equals(that.customer) : that.customer != null) return false;
        if (dateCreated != null ? !dateCreated.equals(that.dateCreated) : that.dateCreated != null) return false;
        if (!id.equals(that.id)) return false;
        if (orderedProducts != null ? !orderedProducts.equals(that.orderedProducts) : that.orderedProducts != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id.hashCode();
        result = 31 * result + (amount != null ? amount.hashCode() : 0);
        result = 31 * result + (dateCreated != null ? dateCreated.hashCode() : 0);
        result = 31 * result + (confirmationNumber != null ? confirmationNumber.hashCode() : 0);
        result = 31 * result + (orderedProducts != null ? orderedProducts.hashCode() : 0);
        result = 31 * result + (customer != null ? customer.hashCode() : 0);
        return result;
    }
}
