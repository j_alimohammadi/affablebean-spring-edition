package affablebean.business.entity;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by javad on 1/2/2015.
 */
@Entity
@Table(name = "customer", schema = "affablebean")
public class Customer {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "name", nullable = false, insertable = true, updatable = true, length = 45)
    private String name;

    @Column(name = "email", nullable = false, insertable = true, updatable = true, length = 45)
    private String email;

    @Column(name = "phone", nullable = false, insertable = true, updatable = true, length = 45)
    private String phone;

    @Column(name = "address", nullable = false, insertable = true, updatable = true, length = 45)
    private String address;

    @Column(name = "city_region", nullable = false, insertable = true, updatable = true, length = 2)
    private String cityRegion;

    @Column(name = "cc_number", nullable = false, insertable = true, updatable = true, length = 50)
    private String ccNumber;

    @OneToMany(mappedBy = "customer")
    private Set<CustomerOrder> customerOrders = new HashSet<>();


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCityRegion() {
        return cityRegion;
    }

    public void setCityRegion(String cityRegion) {
        this.cityRegion = cityRegion;
    }

    public String getCcNumber() {
        return ccNumber;
    }

    public void setCcNumber(String ccNumber) {
        this.ccNumber = ccNumber;
    }

    public Set<CustomerOrder> getCustomerOrders() {
        return customerOrders;
    }

    public void setCustomerOrders(Set<CustomerOrder> customerOrders) {
        this.customerOrders = customerOrders;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Customer customer = (Customer) o;

        if (address != null ? !address.equals(customer.address) : customer.address != null) return false;
        if (ccNumber != null ? !ccNumber.equals(customer.ccNumber) : customer.ccNumber != null) return false;
        if (cityRegion != null ? !cityRegion.equals(customer.cityRegion) : customer.cityRegion != null) return false;
        if (customerOrders != null ? !customerOrders.equals(customer.customerOrders) : customer.customerOrders != null)
            return false;
        if (email != null ? !email.equals(customer.email) : customer.email != null) return false;
        if (!id.equals(customer.id)) return false;
        if (name != null ? !name.equals(customer.name) : customer.name != null) return false;
        if (phone != null ? !phone.equals(customer.phone) : customer.phone != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id.hashCode();
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (email != null ? email.hashCode() : 0);
        result = 31 * result + (phone != null ? phone.hashCode() : 0);
        result = 31 * result + (address != null ? address.hashCode() : 0);
        result = 31 * result + (cityRegion != null ? cityRegion.hashCode() : 0);
        result = 31 * result + (ccNumber != null ? ccNumber.hashCode() : 0);
        result = 31 * result + (customerOrders != null ? customerOrders.hashCode() : 0);
        return result;
    }
}
