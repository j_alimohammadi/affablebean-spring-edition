/*
SQLyog Ultimate v9.51 
MySQL - 5.5.27 : Database - affablebean
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE = '' */;

/*!40014 SET @OLD_UNIQUE_CHECKS = @@UNIQUE_CHECKS, UNIQUE_CHECKS = 0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS = @@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS = 0 */;
/*!40101 SET @OLD_SQL_MODE = @@SQL_MODE, SQL_MODE = 'NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES = @@SQL_NOTES, SQL_NOTES = 0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS */`affablebean` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `affablebean`;

/*Table structure for table `category` */

DROP TABLE IF EXISTS `category`;

CREATE TABLE `category` (
  `id`          BIGINT(20)   NOT NULL AUTO_INCREMENT,
  `img_address` VARCHAR(500) NOT NULL,
  `name`        VARCHAR(45)  NOT NULL,
  PRIMARY KEY (`id`)
)
  ENGINE = InnoDB
  AUTO_INCREMENT = 5
  DEFAULT CHARSET = utf8;

/*Data for the table `category` */

INSERT INTO `category` (`id`, `img_address`, `name`)
VALUES (1, '/img/categories/dairy.jpg', 'dairy'), (2, '/img/categories/meats.jpg', 'meats'),
  (3, '/img/categories/bakery.jpg', 'bakery'), (4, '/img/categories/fruit_&_veg.jpg', 'fruit & veg');

/*Table structure for table `customer` */

DROP TABLE IF EXISTS `customer`;

CREATE TABLE `customer` (
  `id`          INT(11)     NOT NULL AUTO_INCREMENT,
  `address`     VARCHAR(45) NOT NULL,
  `cc_number`   VARCHAR(50) NOT NULL,
  `city_region` VARCHAR(2)  NOT NULL,
  `email`       VARCHAR(45) NOT NULL,
  `name`        VARCHAR(45) NOT NULL,
  `phone`       VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

/*Data for the table `customer` */

/*Table structure for table `customer_order` */

DROP TABLE IF EXISTS `customer_order`;

CREATE TABLE `customer_order` (
  `id`                  INT(11)       NOT NULL AUTO_INCREMENT,
  `amount`              DECIMAL(2, 0) NOT NULL,
  `confirmation_number` INT(11)       NOT NULL,
  `date_created`        DATETIME      NOT NULL,
  `customer_id`         INT(11)                DEFAULT NULL,
  PRIMARY KEY (`id`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

/*Data for the table `customer_order` */

/*Table structure for table `ordered_product` */

DROP TABLE IF EXISTS `ordered_product`;

CREATE TABLE `ordered_product` (
  `customer_order_id` INT(11)     NOT NULL,
  `product_id`        INT(11)     NOT NULL,
  `quantity`          SMALLINT(6) NOT NULL,
  PRIMARY KEY (`customer_order_id`, `product_id`),
  KEY `FK_omtyevy1meqs0x3045aen6gu9` (`customer_order_id`),
  CONSTRAINT `FK_omtyevy1meqs0x3045aen6gu9` FOREIGN KEY (`customer_order_id`) REFERENCES `customer_order` (`id`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

/*Data for the table `ordered_product` */

/*Table structure for table `product` */

DROP TABLE IF EXISTS `product`;

CREATE TABLE `product` (
  `id`          INT(11)        NOT NULL AUTO_INCREMENT,
  `description` VARCHAR(255)            DEFAULT NULL,
  `last_update` DATETIME       NOT NULL,
  `name`        VARCHAR(45)    NOT NULL,
  `price`       DECIMAL(10, 2) NOT NULL,
  `category_id` BIGINT(20)     NOT NULL,
  `img_address` VARCHAR(500)   NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_rlaghtegr0yx2c1q1s6nkqjlh` (`category_id`),
  CONSTRAINT `FK_rlaghtegr0yx2c1q1s6nkqjlh` FOREIGN KEY (`category_id`) REFERENCES `category` (`id`)
)
  ENGINE = InnoDB
  AUTO_INCREMENT = 16
  DEFAULT CHARSET = utf8;

/*Data for the table `product` */

INSERT INTO `product` (`id`, `description`, `last_update`, `name`, `price`, `category_id`, `img_address`)
VALUES (1, 'mild cheddar (330g)', '2015-06-25 11:06:39', 'cheese', '2.23', 1, '/img/products/cheese.png'),
  (2, 'unsalted (250g)', '2015-06-25 11:06:39', 'butter', '4.50', 1, '/img/products/butter.png'),
  (3, 'medium-sized (6 eggs)', '2015-06-25 11:06:39', 'free_range_eggs', '5.33', 1, '/img/products/free_range_eggs.png'),
  (4, 'rolled in fresh herbs<br>2 patties (250g)', '2015-06-25 11:06:39', 'organic_meat_patties', '2.19', 2, '/img/products/organic_meat_patties.png'),
  (5, 'matured, organic (70g)', '2015-06-25 11:06:39', 'parma_ham', '1.19', 2, '/img/products/parma_ham.png'),
  (6, 'free range (250g)', '2015-06-25 11:06:39', 'chicken_leg', '2.12', 2, '/img/products/chicken_leg.png'),
  (7, 'reduced fat, pork<br>3 sausages (350g)', '2015-06-25 11:06:39', 'sausages', '3.15', 2, '/img/products/sausages.png'),
  (8, '600g', '2015-06-25 11:06:39', 'sunflower_seed_loaf', '4.12', 3, '/img/products/sunflower_seed_loaf.png'),
  (9, '4 bagels', '2015-06-25 11:06:39', 'sesame_seed_bagel', '1.11', 3, '/img/products/sesame_seed_bagel.png'),
  (10, '4 buns', '2015-06-25 11:06:39', 'pumpkin_seed_bun', '2.11', 3, '/img/products/pumpkin_seed_bun.png'),
  (11, 'contain peanuts<br>(3 cookies)', '2015-06-25 11:06:39', 'chocolate_cookies', '3.45', 3, '/img/products/chocolate_cookies.png'),
  (12, '2 pieces', '2015-06-25 11:06:39', 'corn_on_the_cob', '5.12', 4, '/img/products/corn_on_the_cob.png'),
  (13, '150g', '2015-06-25 11:06:39', 'red_currants', '6.11', 4, '/img/products/red_currants.png'),
  (14, '500g', '2015-06-25 11:06:39', 'broccoli', '1.50', 4, '/img/products/broccoli.png'),
  (15, '250g', '2015-06-25 11:06:39', 'seedless_watermelon', '2.10', 4, '/img/products/seedless_watermelon.png');

/*!40101 SET SQL_MODE = @OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS = @OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS = @OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES = @OLD_SQL_NOTES */;
